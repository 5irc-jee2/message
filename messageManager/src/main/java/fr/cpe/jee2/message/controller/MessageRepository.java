package fr.cpe.jee2.message.controller;

import fr.cpe.jee2.message.model.MessageModel;
import org.springframework.data.repository.CrudRepository;

public interface MessageRepository extends CrudRepository<MessageModel, Integer> {
    public Iterable<MessageModel> findTop30ByRoomNameOrderByDateDesc(String roomName);
}
