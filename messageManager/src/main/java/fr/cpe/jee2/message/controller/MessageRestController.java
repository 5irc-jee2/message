package fr.cpe.jee2.message.controller;

import java.util.List;

import fr.cpe.jee2.message.controller.MessageService;
import fr.cpe.jee2.message.model.MessageModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class MessageRestController {
	
	@Autowired
	private MessageService messageService;
	
	@RequestMapping("/message/{roomName}")
	private List<MessageModel> getMessages(@PathVariable String roomName) {
		List<MessageModel> rmessage;
		rmessage= messageService.getAllMessageRoom(roomName);
		System.out.println(rmessage.toString());
		return rmessage;
	}
}
