package fr.cpe.jee2.message.controller;

import fr.cpe.jee2.emitter.controller.BusService;
import fr.cpe.jee2.message.model.MessageModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MessageService {

	@Autowired
	private MessageRepository messageRepository;

	public List<MessageModel> getAllMessageRoom(String roomName){
		List<MessageModel> listMessage = new ArrayList<MessageModel>();
		messageRepository.findTop30ByRoomNameOrderByDateDesc(roomName).forEach(listMessage::add);
		return listMessage;
	}

	public void writeMessage(MessageModel messageModel){
		System.out.println(messageModel.toString());
		messageRepository.save(messageModel);
	}

}
