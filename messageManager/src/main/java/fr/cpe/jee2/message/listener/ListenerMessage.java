package fr.cpe.jee2.message.listener;

import fr.cpe.jee2.dto.EnveloppeDTO;
import fr.cpe.jee2.message.controller.MessageService;
import fr.cpe.jee2.message.model.MessageModel;
import fr.cpe.jee2.receiver.controller.BusListener;
import org.apache.activemq.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class ListenerMessage{

    @Autowired
    JmsTemplate jmsTemplate;

    @Autowired
    private MessageService messageService;

    @JmsListener(destination = "${spring.queue.name}")
    public void receiveMessage(MessageModel pMessageModel, Message message) {
        System.out.println("[BUSLISTENER] RECEIVED String MSG=[" + pMessageModel.toString() + "]");
        this.parseData(pMessageModel);
    }

    private void parseData(MessageModel messageModel) {
        messageService.writeMessage(messageModel);
    }
}
