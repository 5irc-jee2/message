package fr.cpe.jee2.message.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class MessageModel implements Serializable {

	private static final long serialVersionUID = 2733795832476568049L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	private String roomName;
	private int userId;
	private String message;
	private Date date;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@java.lang.Override
	public java.lang.String toString() {
		final java.lang.StringBuilder stringBuilder = new java.lang.StringBuilder("UserModel{");
		stringBuilder.append("id=").append(id);
		stringBuilder.append(", roomName='").append(roomName).append('\'');
		stringBuilder.append(", userId=").append(userId);
		stringBuilder.append(", message='").append(message).append('\'');
		stringBuilder.append(", date=").append(date);
		stringBuilder.append('}');
		return stringBuilder.toString();
	}
}
